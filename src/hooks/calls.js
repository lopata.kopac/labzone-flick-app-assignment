import { useState, useEffect } from "react";
import axios from "axios";

function FetchHook(url, datamapper) {
  const [response, setResponse] = useState({
    data: null,
    loading: false,
    error: null,
    success: false
  });

  if (datamapper == null || datamapper === undefined) {
    datamapper = data => data;
  }

  useEffect(() => {
    setResponse({ ...response, loading: true });

    axios.get(url)
      .then(data => datamapper(data))
      .then(data => { setResponse({ ...response, loading: false, error: null, success: true, data: data }); })
      .catch(error => { setResponse({ ...response, loading: false, success: false, error: error, data: null, }); });

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return response;
}

export default FetchHook;