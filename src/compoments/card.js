import styled from "styled-components";
import Button from "./button";
import PropTypes from 'prop-types';

var CardBody = styled.div`
    padding: 20px 24px;
`;

var CardText = styled.div`
    margin: 20px 0px;
`;

var CardWrapper = styled.div`
    width: 97.5%;
    margin 2.5%;
    box-sizing: border-box;
    display: inline-block;
    vertical-align: top;

    box-shadow: 0px 10px 14px rgba(0, 0, 0, 0.08);
    border-radius: 3px;
    background: #fff;

    img {
        border-radius: 3px 3px 0px 0px;
        width: 100%; 
        max-height: 165px;
        object-fit: cover;
    }

    @media only screen and (min-width: 768px){
        width: 28%;
    }
`;

var CardTittle = styled.h2`
    margin: 0px;
    font-size: 1.75rem;
`;

var Loading = styled.h2`
    display: flex;
    justify-content: center;
    padding: 20px;
`;

function Card({ children, tittle, img, alt, buttonText, onClick, loading }) {

    if (loading) {
        return (
            <CardWrapper>
                <Loading>Loading</Loading>
            </ CardWrapper>
        );
    }

    return (
        <CardWrapper>
            <img alt={alt} src={img} />
            <CardBody>
                <CardTittle>{tittle}</CardTittle>
                <CardText>{children}</CardText>
                <Button onClick={onClick}>{buttonText}</Button>
            </CardBody>
        </ CardWrapper>
    );
}

Card.propTypes = {
    tittle: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    img: PropTypes.string,
    alt: PropTypes.string,
    buttonText: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    onClick: PropTypes.func,
    loading: PropTypes.bool
}

Card.defaultProps = {
    buttonText: 'Explore ➔',
    loading: false
};

export default Card;