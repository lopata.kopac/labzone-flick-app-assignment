import styled from "styled-components";

var Button = styled.button`
    padding: 12px;
    width: 100%;
    box-sizing: border-box;

    border: 1px solid transparent;
    border-radius: 3px;

    background: background: #f5f5f5;
    color: #0b868b;

    font-size: 1.0rem;

    :hover{
        border: 1px solid #0B868B;
        cursor: pointer;
    }
`;

export default Button;