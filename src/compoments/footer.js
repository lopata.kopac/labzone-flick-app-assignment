import styled from "styled-components";

var Footer = styled.header`
    position: fixed;
    bottom: 0;
    width: 100%;
    box-sizing: border-box;

    display: flex;
    justify-content: center;

    padding: 22px;
    background:  #0b868b;

    color: #FFFFFF;
    font-size: 1.0rem;
`;

export default Footer;

