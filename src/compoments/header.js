import styled from "styled-components";

var Header = styled.header`
    position: fixed;
    top: 0;
    width: 100%;
    box-sizing: border-box;

    padding: 14px 24px;
    background: #2c2c2c;

    color: #fff;
    font-size: 1.75rem;
`;

export default Header;