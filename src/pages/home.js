import React from 'react';
import Header from '../compoments/header';
import Footer from '../compoments/footer';
import Card from '../compoments/card';
import CardBox from '../compoments/card-box';
import styled from 'styled-components';
import { randomNumber, capitalizeFirstLetter } from '../until/until';

import callGet from '../hooks/calls';

var Background = styled.div`
    min-height: 100vh;
    min-width: 100vw;
    position: absolute;
    background: #efefef;
`;

const pokemonDataMapper = data => { return { id: data?.data?.id, name: capitalizeFirstLetter(data?.data?.name), image: data?.data?.sprites?.front_default } };

var pokemonApiUrl = 'https://pokeapi.co/api/v2/pokemon/';

function Home() {
    const pokemons = [
        callGet(pokemonApiUrl + randomNumber(0, 150), pokemonDataMapper),
        callGet(pokemonApiUrl + randomNumber(0, 150), pokemonDataMapper),
        callGet(pokemonApiUrl + randomNumber(0, 150), pokemonDataMapper),
    ]

    const description = [
        'Look up at the night sky, and find yoursel immersed in the amazing mountain range of Alps.',
        'Capture the stunning essence of the early morning sunrise in the Californian wilderness.',
        'Sunsets over the stunning Utah Canyonlands, is truly something much more than incredible.'
    ]

    return (
        <Background>
            <Header>Flicp App</Header>
            <CardBox>
                {pokemons.map((item, index) => {
                    return (
                        <Card tittle={item.data?.name}
                            key={index}
                            img={item.data?.image}
                            loading={item.loading}
                            onClick={() => { console.log('Ankcion') }}
                        >
                            {description[index]}
                        </Card>
                    );
                })}
            </CardBox>
            <Footer>Images are from Flickr.</Footer>
        </ Background>
    );
}

export default Home;