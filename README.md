# Flock App for LabZone
This is test assignment for Labzone.

Aplikacion display images and description from public API.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### Demo

You can show demo app on: [https://blissful-mestorf-b3ed91.netlify.app/](https://blissful-mestorf-b3ed91.netlify.app/)

